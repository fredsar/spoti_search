package main

import (
	"context"
	"os"
	"os/signal"
	"time"

	"github.com/labstack/echo"

	"gitlab.com/fredsar/spoti_search/spotify"
)

const (
	// ScopeUserReadPrivate seeks read access to a user's
	// subsription details (type of user account).
	ScopeUserReadPrivate = "user-read-private"
	redirectURL          = "http://localhost:8080/callback"
)

func main() {
	e := echo.New()
	spotify.InitializeAuthenticator(
		[]string{ScopeUserReadPrivate},
		redirectURL,
	)

	e.GET("/callback", spotify.Authorize)
	e.GET("/search", spotify.Search)
	e.GET("/playlist/:playlistID",spotify.GetPlaylist)

	// Start server
	go func() {
		if err := e.Start(":8080"); err != nil {
			e.Logger.Info("shutting down the server")
		}
	}()

	// Handle graceful shutdown
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}

}
