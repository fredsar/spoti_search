package spotify

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
	"golang.org/x/oauth2"
)

const (
	spotifyID     = ""
	spotifySecret = ""

	state = "123456"
)

var (
	authenticator *oauth2.Config
	spoToken      *oauth2.Token
)

func InitializeAuthenticator(
	scopes []string,
	redirectURL string,
) {

	authenticator = &oauth2.Config{
		ClientID:     spotifyID,
		ClientSecret: spotifySecret,
		RedirectURL:  redirectURL,
		Scopes:       scopes,
		Endpoint: oauth2.Endpoint{
			AuthURL:  "https://accounts.spotify.com/authorize",
			TokenURL: "https://accounts.spotify.com/api/token",
		},
	}
	url := authenticator.AuthCodeURL(state)
	fmt.Println(fmt.Sprintf("If you want to login, please go to %s", url))
}

func Authorize(c echo.Context) error {
	code := c.QueryParam("code")
	token, err := authenticator.Exchange(c.Request().Context(), code)
	if err != nil {
		log.Errorf("error exchanging token")
		return c.JSON(http.StatusInternalServerError, "some error")
	}
	spoToken = token
	return c.JSON(http.StatusOK, token)
}
