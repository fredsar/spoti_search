module gitlab.com/fredsar/spoti_search

go 1.13

require (
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0
	golang.org/x/crypto v0.0.0-20200427165652-729f1e841bcc // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
)
